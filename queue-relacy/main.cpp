#include <relacy/relacy_std.hpp>
#include "queue.h"

using TestType = rl::var<int>;
constexpr int TestSize = 1;

struct race_test : rl::test_suite<race_test, 2>
{

    void before()
    {
        q = new queue3<TestType,TestSize>();
    }

    // main thread function
    void thread(unsigned thread_index)
    {
        if (0 == thread_index)
        {
            t1();
        }
        else
        {
            t2();
        }
    }

    // executed in single thread after main thread function
    void after()
    {
        delete q;
        q = nullptr;
    }

    // executed in single thread after every 'visible' action in main threads
    // disallowed to modify any state
    void invariant()
    { }

    queue3<TestType,TestSize>* q;

    void t2()
    {
        for(int i = 0; i<20; i++)
        {
            TestType j=-1;
            while(!q->pop_t2(j));
            assert(i == j($));
        }
    }

    void t1()
    {
        for(int i = 0; i<20; i++)
        {
            while(!q->push_t1(i));
        }
    }

};



int main()
{
    rl::simulate<race_test>();
}
