#ifndef LOCK_FREE_QUEUE_H
#define LOCK_FREE_QUEUE_H


// android 1: 18s, 2: 18s, 3: 6.5s, lock: 77s, relaxed: rossz

static constexpr int CacheLineSize = 64;

template<typename T, int Capacity>
class queue3
{
    T mData[Capacity+1];
    alignas(CacheLineSize) rl::atomic<int> mBase;
    alignas(CacheLineSize) rl::atomic<int> mNext;
public:
    queue3()
    {
        mBase($) = 0;
        mNext($) = 0;
    }
    bool push_t1(const T& value)
    {
        int base = mBase.load(rl::memory_order_relaxed);
        int next = mNext.load(rl::memory_order_relaxed);
        int before_base = wrap(base-1);
        if(next == before_base)
            return false;
        mData[next]($) = value($);
        mNext.store(wrap(next+1),rl::memory_order_release); //release
        return true;
    }
    bool pop_t2(T& out)
    {
        int base = mBase.load(rl::memory_order_relaxed);
        int next = mNext.load(rl::memory_order_acquire); //acquire
        if(base == next)
            return false;
        out($) = mData[base]($);
        mBase.store(wrap(base+1), rl::memory_order_relaxed);
        return true;
    }
private:
    static int wrap( int i )
    {
        while(i>=Capacity+1)
            i-=(Capacity+1);
        while(i<0)
            i+=(Capacity+1);
        return i;
    }
};

template<typename T, int Capacity>
class queue_with_mutex
{
    T mData[Capacity+1];
    rl::mutex mutex;
    rl::var<int> mBase;
    rl::var<int> mNext;
public:
    queue_with_mutex()
    : mBase(0)
    , mNext(0)
    {}
    bool push_t1(const T& value)
    {
        mutex.lock($);
        int before_base = wrap(mBase($)-1);
        if(mNext($) == before_base)
        {
            mutex.unlock($);
            return false;
        }
        mData[mNext($)]($) = value($);
        mNext($) = wrap(mNext($)+1);
        mutex.unlock($);
        return true;
    }
    bool pop_t2(T& out)
    {
        mutex.lock($);
        if(mBase($) == mNext($))
        {
            mutex.unlock($);
            return false;
        }
        out($) = mData[mBase($)]($);
        mBase($) = wrap(mBase($)+1);
        mutex.unlock($);
        return true;
    }
private:
    static int wrap( int i )
    {
        while(i>=Capacity+1)
        {
            i-=(Capacity+1);
        }
        while(i<0)
        {
            i+=(Capacity+1);
        }
        return i;
    }
};




/*
template<typename T, int Capacity>
class queue4
{
    T mData[Capacity+1];
    alignas(CacheLineSize) std::atomic<int> mBase;
    alignas(CacheLineSize) std::atomic<int> mNext;
public:
    queue4()
    : mBase(0)
    , mNext(0)
    {}
    bool push_t1(const T& value)
    {
        int base = mBase.load(std::memory_order_relaxed);
        int next = mNext.load(std::memory_order_relaxed);
        int beforemBase = wrap(base-1);
        if(next == beforemBase)
            return false;
        mData[next] = value;
        mNext.store(wrap(next+1),std::memory_order_release); //release
        return true;
    }
    bool pop_t2(T& out)
    {
        int base = mBase.load(std::memory_order_relaxed);
        int next = mNext.load(std::memory_order_relaxed); //acquire
        if(base == next)
            return false;
        std::atomic_thread_fence(std::memory_order_acquire);
        out = mData[base];
        mBase.store(wrap(base+1), std::memory_order_relaxed);
        return true;
    }
private:
    static int wrap( int i )
    {
        while(i>=Capacity+1)
            i-=(Capacity+1);
        while(i<0)
            i+=(Capacity+1);
        return i;
    }
};
*/


#endif //LOCK_FREE_QUEUE_H
