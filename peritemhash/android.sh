#!/bin/bash
# ~/apps/android-sdk/ndk-bundle/build/tools/make-standalone-toolchain.sh --arch=arm --install-dir=/home/tomi/arm-gcc-4.9 --toolchain=arm-linux-androideabi-4.9
# http://developer.android.com/ndk/guides/standalone_toolchain.html
~/apps/arm-gcc-4.9/bin/arm-linux-androideabi-g++  -DREADER_THREADS=3 -O2 -fPIE -pie  main.cpp -pthread -std=c++14 -o lockfree-arm
~/apps/android-sdk/platform-tools/adb push lockfree-arm /data/local/tmp
~/apps/android-sdk/platform-tools/adb push repeat.sh /data/local/tmp
~/apps/android-sdk/platform-tools/adb shell "cd /data/local/tmp/; time sh ./repeat.sh 10 ./lockfree-arm"
