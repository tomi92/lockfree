#ifndef HASHTABLE_H
#define HASHTABLE_H
#include <vector>
#include <forward_list>
#include <algorithm>
#include <shared_mutex>
#include <atomic>

template<typename ForwardList, typename Pred>
bool forward_list_remove_if(ForwardList& list, Pred pred)
{
    auto prevIt = std::adjacent_find(list.before_begin(), list.end(),
        [pred](const auto&, const auto& curr){ return pred(curr); });

    bool found = prevIt != list.end();

    if(found)
        list.erase_after(prevIt);

    return found;
}

template<typename ForwardList>
auto forward_list_last(ForwardList& list)
{
    auto it = list.before_begin();
    while(std::next(it)!=list.end())
        ++it;
    return it;
}

template<typename ForwardList>
void forward_list_move_first(ForwardList& from, ForwardList& to)
{
    to.splice_after(to.before_begin(), from, from.before_begin());
}

template<typename K, typename V>
class hash_table
{
    using SharedMutex = std::shared_timed_mutex; // todo change to shared_mutex when c++17 support is around
    using SharedLock = std::shared_lock<SharedMutex>;
    using UniqueLock = std::unique_lock<SharedMutex>;
    using KV = std::pair<K,V>;

    struct Bucket
    {
        std::forward_list<KV> list;
        SharedMutex mutex;
    };

    using Table = std::vector<Bucket>;

    static constexpr size_t MinTableSize = 16;

    struct KeyEquals
    {
        K key;

        KeyEquals(K _key)
            : key(_key)
        {}

        bool operator()(const KV& kv) const
        {
            return kv.first == key;
        }
    };

    std::hash<K> hash_;
    Table table_;
    mutable SharedMutex mutex_;
    std::atomic<size_t> size_;

    Bucket& getBucketOfImpl(const K& key)
    {
        return table_[hash_(key) % table_.size()];
    }

    const Bucket& getBucketOfImpl(const K& key) const
    {
        return table_[hash_(key) % table_.size()];
    }


    bool overPopulatedImpl()
    {
        return size_.load() >= table_.size() * 0.7;
    }

    bool underPopulatedImpl()
    {
        size_t size = size_.load();
        return size <= table_.size() * 0.2 && size > MinTableSize;
    }

    void shrinkToHalfImpl()
    {
        assert(table_.size()/2 >= 1);
        Table newTable(table_.size()/2);
        for(size_t i = 0; i<newTable.size(); ++i)
        {
            auto& newList = newTable[i].list;
            auto& oldList1 = table_[i].list;
            auto& oldList2 = table_[i+newTable.size()].list;

            newList.swap(oldList1);
            newList.splice_after(forward_list_last(newList), oldList2);
        }
        table_.swap(newTable);
    }

    void expandToDoubleImpl()
    {
        assert(table_.size() <= std::numeric_limits<size_t>::max() / 2);
        Table newTable(table_.size()*2);

        for(Bucket& bucketFrom: table_)
        {
            auto& listFrom = bucketFrom.list;
            while(!listFrom.empty())
            {
                auto& listTo = newTable[hash_(listFrom.begin()->first) % newTable.size()].list;
                forward_list_move_first(listFrom, listTo);
            }
        }
        table_.swap(newTable);
    }

public:

    hash_table(int initialBuckets = MinTableSize) // todo feltétel a méretre
        : table_(initialBuckets)
    {}

    ~hash_table()
    {}

    const V* get(const K& key) const
    {
        SharedLock slock(mutex_);
        auto& list = getBucketOfImpl(key).list;
        auto it = std::find_if(list.begin(), list.end(), KeyEquals(key));
        return it == list.end() ? nullptr : &it->second;
    }

    void set(const K& key, const V& value)
    {
        bool isOverPopulated; // calculated with shared lock held

        {
            SharedLock slock(mutex_);
            auto& bucket = getBucketOfImpl(key);
            UniqueLock ulock(bucket.mutex);
            auto& list = bucket.list;
            auto it = std::find_if(list.begin(), list.end(), KeyEquals(key));
            if(it != list.end())
            {
                *it = KV(key,value);
            }
            else
            {
                size_.fetch_add(1);
                list.emplace_front(key,value);
            }
            isOverPopulated = overPopulatedImpl();
        }

        if(isOverPopulated)
        {
            UniqueLock ulock(mutex_);
            if(overPopulatedImpl())
                expandToDoubleImpl();
        }
    }

    void remove(const K& key)
    {
        bool isUnderPopulated; // calculated with shared lock held

        {
            SharedLock slock(mutex_);
            auto& bucket = getBucketOfImpl(key);
            UniqueLock ulock(bucket.mutex);
            bool found = forward_list_remove_if(bucket.list, KeyEquals(key));
            if(found)
                size_.fetch_sub(1);
            isUnderPopulated = underPopulatedImpl();
        }

        if(isUnderPopulated)
        {
            UniqueLock ulock(mutex_);
            if(underPopulatedImpl())
                shrinkToHalfImpl();
        }
    }
};

#endif // HASHTABLE_H
