#ifndef LOCK_FREE_QUEUE_H
#define LOCK_FREE_QUEUE_H
#include <atomic>
#include <mutex>
#include <thread>

static constexpr int CacheLineSize = 64;

template <typename T>
struct alignas(CacheLineSize) aligned_wrapper
{
    T value;
};

inline void spin(int n)
{
    for(volatile int i=n;i--;);
}

inline int cyclic_next( int i, int m )
{
    if(i == m-1)
        return 0;
    return i + 1;
}

inline int cyclic_prev( int i, int m )
{
    if(i == 0)
        return m-1;
    return i - 1;
}

template<typename T, int C>
class stqueue
{
public:
    typedef T value_type;
    static constexpr int capacity = C;
    static constexpr int arraySize = capacity + 1;
private:
    value_type mData[arraySize];
    int mBase;
    int mNext;
public:
    stqueue()
    : mBase(0)
    , mNext(0)
    {}
    bool push_t1(const value_type& value)
    {
        int before_base = cyclic_prev(mBase, arraySize);
        if(mNext == before_base)
        {
            return false;
        }
        mData[mNext] = value;
        mNext = cyclic_next(mNext, arraySize);
        return true;
    }
    bool pop_t2(value_type& out)
    {
        if(mBase == mNext)
        {
            return false;
        }
        out = mData[mBase];
        mBase = cyclic_next(mBase, arraySize);
        return true;
    }
};

template<typename T, int C>
class lfqueue1
{
public:
    typedef T value_type;
    static constexpr int capacity = C;
    static constexpr int arraySize = capacity + 1;
private:
    value_type mData[arraySize];
    std::atomic<int> mBase;
    std::atomic<int> mNext;
public:
    lfqueue1()
    : mBase(0)
    , mNext(0)
    {}
    bool push_t1(const value_type& value)
    {
        int base = mBase.load();
        int next = mNext.load();
        int before_base = cyclic_prev(base, arraySize);
        if(next == before_base)
        {
            return false;
        }
        mData[next] = value;
        mNext.store(cyclic_next(next, arraySize));
        return true;
    }
    bool pop_t2(value_type& out)
    {
        int base = mBase.load();
        int next = mNext.load();
        if(base == next)
        {
            return false;
        }
        out = mData[base];
        mBase.store(cyclic_next(base, arraySize));
        return true;
    }
};

template<typename T, int C>
class lfqueue2
{
public:
    typedef T value_type;
    static constexpr int capacity = C;
    static constexpr int arraySize = capacity + 1;
private:
    value_type mData[arraySize];
    alignas(CacheLineSize) std::atomic<int> mBase;
    alignas(CacheLineSize) std::atomic<int> mNext;
public:
    lfqueue2()
    : mBase(0)
    , mNext(0)
    {}
    bool push_t1(const value_type& value)
    {
        int base = mBase.load();
        int next = mNext.load();
        int before_base = cyclic_prev(base, arraySize);
        if(next == before_base)
        {
            return false;
        }
        mData[next] = value;
        mNext.store(cyclic_next(next, arraySize));
        return true;
    }
    bool pop_t2(value_type& out)
    {
        int base = mBase.load();
        int next = mNext.load();
        if(base == next)
        {
            return false;
        }
        out = mData[base];
        mBase.store(cyclic_next(base, arraySize));
        return true;
    }
};

template<typename T, int C>
class lfqueue3
{
public:
    typedef T value_type;
    static constexpr int capacity = C;
    static constexpr int arraySize = capacity + 1;
private:
    value_type mData[arraySize];
    alignas(CacheLineSize) std::atomic<int> mBase;
    alignas(CacheLineSize) std::atomic<int> mNext;
public:
    lfqueue3()
    : mBase(0)
    , mNext(0)
    {}
    bool push_t1(const value_type& value)
    {
        int base = mBase.load(std::memory_order_acquire);
        int next = mNext.load(std::memory_order_relaxed);
        int before_base = cyclic_prev(base, arraySize);
        if(next == before_base)
        {
            return false;
        }
        mData[next] = value;
        mNext.store(cyclic_next(next, arraySize), std::memory_order_release);
        return true;
    }
    bool pop_t2(value_type& out)
    {
        int base = mBase.load(std::memory_order_relaxed);
        int next = mNext.load(std::memory_order_acquire);
        if(base == next)
        {
            return false;
        }
        out = mData[base];
        mBase.store(cyclic_next(base, arraySize), std::memory_order_release);
        return true;
    }
};

template<typename T, int C>
class lfqueue4
{
public:
    typedef T value_type;
    static constexpr int capacity = C;
    static constexpr int arraySize = capacity + 1;
private:
    value_type mData[arraySize];
    alignas(CacheLineSize) std::atomic<int> mBase;
    alignas(CacheLineSize) std::atomic<int> mNext;
public:
    lfqueue4()
    : mBase(0)
    , mNext(0)
    {}
    bool push_t1(const value_type& value)
    {
        int base = mBase.load(std::memory_order_relaxed);
        int next = mNext.load(std::memory_order_relaxed);
        int before_base = cyclic_prev(base, arraySize);
        if(next == before_base)
        {
            return false;
        }
        std::atomic_thread_fence(std::memory_order_acquire);
        mData[next] = value;
        mNext.store(cyclic_next(next, arraySize), std::memory_order_release);
        return true;
    }
    bool pop_t2(value_type& out)
    {
        int base = mBase.load(std::memory_order_relaxed);
        int next = mNext.load(std::memory_order_relaxed);
        if(base == next)
        {
            return false;
        }
        std::atomic_thread_fence(std::memory_order_acquire);
        out = mData[base];
        mBase.store(cyclic_next(base, arraySize), std::memory_order_release);
        return true;
    }
};

template<typename T, int C>
class lfqueue5
{
public:
    typedef T value_type;
    static constexpr int capacity = C;
    static constexpr int arraySize = capacity;
private:
    struct Item
    {
        std::atomic<bool> hasData;
        value_type data;
        Item()
        : hasData(false)
        {}
    };
    Item mItems[arraySize];
    alignas(CacheLineSize) int mBase;
    alignas(CacheLineSize) int mNext;
public:
    lfqueue5()
    : mBase(0)
    , mNext(0)
    {}
    bool push_t1(const value_type& value)
    {
        if(mItems[mNext].hasData.load(std::memory_order_acquire))
            return false;
        mItems[mNext].data = value;
        mItems[mNext].hasData.store(true, std::memory_order_release);
        mNext=cyclic_next(mNext, arraySize);
        return true;
    }
    bool pop_t2(value_type& out)
    {
        if(!mItems[mBase].hasData.load(std::memory_order_acquire))
            return false;
        out = mItems[mBase].data;
        mItems[mBase].hasData.store(false, std::memory_order_release);
        mBase=cyclic_next(mBase, arraySize);
        return true;
    }
};

template<typename T, int C>
class lfqueue6
{
public:
    typedef T value_type;
    static constexpr int capacity = C;
    static constexpr int arraySize = capacity;
private:
    struct alignas(CacheLineSize) Item
    {
        std::atomic<bool> hasData;
        value_type data;
        Item()
        : hasData(false)
        {}
    };
    Item mItems[arraySize];
    alignas(CacheLineSize) int mBase;
    alignas(CacheLineSize) int mNext;
public:
    lfqueue6()
    : mBase(0)
    , mNext(0)
    {}
    bool push_t1(const value_type& value)
    {
        if(mItems[mNext].hasData.load(std::memory_order_acquire))
            return false;
        mItems[mNext].data = value;
        mItems[mNext].hasData.store(true, std::memory_order_release);
        mNext=cyclic_next(mNext, arraySize);
        return true;
    }
    bool pop_t2(value_type& out)
    {
        if(!mItems[mBase].hasData.load(std::memory_order_acquire))
            return false;
        out = mItems[mBase].data;
        mItems[mBase].hasData.store(false, std::memory_order_release);
        mBase=cyclic_next(mBase, arraySize);
        return true;
    }
};


template<typename T, int C>
class glqueue
{
public:
    typedef T value_type;
    static constexpr int capacity = C;
    static constexpr int arraySize = capacity + 1;
private:
    value_type mData[arraySize];
    alignas(CacheLineSize) std::mutex mutex;
    alignas(CacheLineSize) int mBase;
    alignas(CacheLineSize) int mNext;
public:
    glqueue()
    : mBase(0)
    , mNext(0)
    {}
    bool push_t1(const value_type& value)
    {
        std::lock_guard<std::mutex> lock(mutex);
        int before_base = cyclic_prev(mBase, arraySize);
        if(mNext == before_base)
        {
            return false;
        }
        mData[mNext] = value;
        mNext = cyclic_next(mNext, arraySize);
        return true;
    }
    bool pop_t2(value_type& out)
    {
        std::lock_guard<std::mutex> lock(mutex);
        if(mBase == mNext)
        {
            return false;
        }
        out = mData[mBase];
        mBase = cyclic_next(mBase, arraySize);
        return true;
    }
};

template<typename T, int C>
class pilqueue1
{
public:
    typedef T value_type;
    static constexpr int capacity = C;
    static constexpr int arraySize = capacity;
private:
    struct Item
    {
        std::mutex mutex;
        bool hasData = false;
        value_type data;
    };
    Item mItems[arraySize];
    int mBase;
    int mNext;
public:
    pilqueue1()
    : mBase(0)
    , mNext(0)
    {}
    bool push_t1(const value_type& value)
    {
        auto& item = mItems[mNext];
        std::lock_guard<std::mutex> lock(item.mutex);
        if(item.hasData)
            return false;
        item.data = value;
        item.hasData = true;
        mNext = cyclic_next(mNext, arraySize);
        return true;
    }
    bool pop_t2(value_type& out)
    {
        auto& item = mItems[mBase];
        std::lock_guard<std::mutex> lock(item.mutex);
        if(!item.hasData)
            return false;
        out = item.data;
        item.hasData = false;
        mBase = cyclic_next(mBase, arraySize);
        return true;
    }
};

template<typename T, int C>
class pilqueue2
{
public:
    typedef T value_type;
    static constexpr int capacity = C;
    static constexpr int arraySize = capacity;
private:
    struct Item
    {
        std::mutex mutex;
        bool hasData = false;
        value_type data;
    };
    Item mItems[arraySize];
    alignas(CacheLineSize) int mBase;
    alignas(CacheLineSize) int mNext;
public:
    pilqueue2()
    : mBase(0)
    , mNext(0)
    {}
    bool push_t1(const value_type& value)
    {
        auto& item = mItems[mNext];
        std::lock_guard<std::mutex> lock(item.mutex);
        if(item.hasData)
            return false;
        item.data = value;
        item.hasData = true;
        mNext = cyclic_next(mNext, arraySize);
        return true;
    }
    bool pop_t2(value_type& out)
    {
        auto& item = mItems[mBase];
        std::lock_guard<std::mutex> lock(item.mutex);
        if(!item.hasData)
            return false;
        out = item.data;
        item.hasData = false;
        mBase = cyclic_next(mBase, arraySize);
        return true;
    }
};


template<typename T, int C>
class pilqueue3
{
public:
    typedef T value_type;
    static constexpr int capacity = C;
    static constexpr int arraySize = capacity;
private:
    struct alignas(CacheLineSize) Item
    {
        std::mutex mutex;
        bool hasData = false;
        value_type data;
    };
    Item mItems[arraySize];
    alignas(CacheLineSize) int mBase;
    alignas(CacheLineSize) int mNext;
public:
    pilqueue3()
    : mBase(0)
    , mNext(0)
    {}
    bool push_t1(const value_type& value)
    {
        auto& item = mItems[mNext];
        std::lock_guard<std::mutex> lock(item.mutex);
        if(item.hasData)
            return false;
        item.data = value;
        item.hasData = true;
        mNext = cyclic_next(mNext, arraySize);
        return true;
    }
    bool pop_t2(value_type& out)
    {
        auto& item = mItems[mBase];
        std::lock_guard<std::mutex> lock(item.mutex);
        if(!item.hasData)
            return false;
        out = item.data;
        item.hasData = false;
        mBase = cyclic_next(mBase, arraySize);
        return true;
    }
};



#endif //LOCK_FREE_QUEUE_H
