#!/bin/bash
# build/tools/make-standalone-toolchain.sh --arch=arm --install-dir=/tmp/ndk-arm
# http://janos.io/articles/cross-compile.html
~/apps/armcc/bin/arm-linux-androideabi-g++ -O2 -fPIE -pie  main.cpp -pthread -std=c++11 -o lockfree-arm
~/apps/android-sdk/platform-tools/adb push lockfree-arm /data/local/tmp
~/apps/android-sdk/platform-tools/adb push repeat.sh /data/local/tmp
i=0
while [ $i -le 10 ]
do
    echo $i
    ~/apps/android-sdk/platform-tools/adb shell "cd /data/local/tmp/; time sh ./repeat.sh 100 ./lockfree-arm $i"
    i=$(($i + 1))
done

