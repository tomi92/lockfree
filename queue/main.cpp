#include <cassert>
#include <iostream>
#include <thread>
#include "queue.h"
#include <memory>
#include <functional>
#include <vector>
#include <sstream>

template<
    template<typename DT, int QS> class QueueType,
    typename DataType = int64_t,
    int QueueSize = 100,
    int Steps = 1000000>
class Test
{
    QueueType<DataType, QueueSize> q;
public:
    void reader()
    {
        for(int i = 0; i<Steps; i++)
        {
            DataType j;
            while(!q.pop_t2(j));
            assert(i == j);
        }
    }

    void writer()
    {
        for(int i = 0; i<Steps; i++)
            while(!q.push_t1(i));
    }

    void run_multi_threaded()
    {
        std::thread readerThread(&Test::reader, this);
        writer();
        readerThread.join();
    }

    void run_single_threaded()
    {
        for(int i = 0; i<Steps/QueueSize; i++)
        {
            for(int j = 0; j<QueueSize; j++)
            {
                q.push_t1(i*QueueSize+j);
            }
            for(int j = 0; j<QueueSize; j++)
            {
                DataType k;
                q.pop_t2(k);
                assert(k == i*QueueSize+j);
            }
        }
    }
};

std::vector<std::function<void()>> tests = {
    [](){ Test<stqueue> t; t.run_single_threaded(); },
    [](){ Test<lfqueue1> t; t.run_multi_threaded(); },
    [](){ Test<lfqueue2> t; t.run_multi_threaded(); },
    [](){ Test<lfqueue3> t; t.run_multi_threaded(); },
    [](){ Test<lfqueue4> t; t.run_multi_threaded(); },
    [](){ Test<lfqueue5> t; t.run_multi_threaded(); },
    [](){ Test<lfqueue6> t; t.run_multi_threaded(); },
    [](){ Test<glqueue> t; t.run_multi_threaded(); },
    [](){ Test<pilqueue1> t; t.run_multi_threaded(); },
    [](){ Test<pilqueue2> t; t.run_multi_threaded(); },
    [](){ Test<pilqueue3> t; t.run_multi_threaded(); }
};

int main(int argc, char* argv[])
{
    assert(argc == 2);
    std::stringstream ss(argv[1]);
    int i;
    assert(ss >> i >> std::ws && ss.eof());
    tests.at(i)();
}
