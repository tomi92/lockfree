#!/bin/bash
g++ -O2 main.cpp -pthread -std=c++11 -o lockfree-x86
i=0
while [ $i -le 10 ]
do
    echo $i
    time sh ./repeat.sh 100 ./lockfree-x86 $i
    i=$(($i + 1))
done


