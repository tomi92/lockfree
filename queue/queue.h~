#ifndef LOCK_FREE_QUEUE_H
#define LOCK_FREE_QUEUE_H
#include <atomic>
#include <mutex>
#include <thread>


// android 1: 18s, 2: 18s, 3: 6.5s, lock: 77s, relaxed: rossz

static constexpr int CacheLineSize = 64;

template<typename T, int C>
class queue
{
public:
    typedef T value_type;
    static constexpr int capacity = C;
private:
    value_type mData[capacity+1];
    std::atomic<int> mBase;
    std::atomic<int> mNext;
public:
    queue()
    : mBase(0)
    , mNext(0)
    {}
    bool push_t1(const value_type& value)
    {
        int base = mBase.load();
        int next = mNext.load();
        int beforemBase = wrap(base-1);
        if(next == beforemBase)
        {
            return false;
        }
        mData[next] = value;
        mNext.store(wrap(next+1)); //release
        return true;
    }
    bool pop_t2(value_type& out)
    {
        int base = mBase.load();
        int next = mNext.load(); //acquire
        if(base == next)
        {
            return false;
        }
        out = mData[base];
        mBase.store(wrap(base+1));
        return true;
    }
private:
    static int wrap( int i )
    {
        while(i>=capacity+1)
        {
            i-=(capacity+1);
        }
        while(i<0)
        {
            i+=(capacity+1);
        }
        return i;
    }
};

template <typename T>
struct alignas(64) aligned_wrapper
{
    T value;
};

template<typename T, int C>
class queue2
{
public:
    typedef T value_type;
    static constexpr int capacity = C;
private:
    value_type mData[capacity+1];
    alignas(CacheLineSize) std::atomic<int> mBase;
    alignas(CacheLineSize) std::atomic<int> mNext;
public:
    queue2()
    : mBase(0)
    , mNext(0)
    {}
    bool push_t1(const value_type& value)
    {
        int base = mBase.load();
        int next = mNext.load();
        int beforemBase = wrap(base-1);
        if(next == beforemBase)
        {
            return false;
        }
        mData[next] = value;
        mNext.store(wrap(next+1)); //release
        return true;
    }
    bool pop_t2(value_type& out)
    {
        int base = mBase.load();
        int next = mNext.load(); //acquire
        if(base == next)
        {
            return false;
        }
        out = mData[base];
        mBase.store(wrap(base+1));
        return true;
    }
private:
    static int wrap( int i )
    {
        while(i>=capacity+1)
        {
            i-=(capacity+1);
        }
        while(i<0)
        {
            i+=(capacity+1);
        }
        return i;
    }
};

template<typename T, int C>
class queue_nosync
{
public:
    typedef T value_type;
    static constexpr int capacity = C;
private:
    value_type mData[capacity+1];
    alignas(CacheLineSize) volatile int mBase;
    alignas(CacheLineSize) volatile int mNext;
public:
    queue_nosync()
    : mBase(0)
    , mNext(0)
    {}
    bool push_t1(const value_type& value)
    {
        int base = mBase;
        int next = mNext;
        int beforemBase = wrap(base-1);
        if(next == beforemBase)
        {
            return false;
        }
        mData[next] = value;
        mNext = wrap(next+1);
        return true;
    }
    bool pop_t2(value_type& out)
    {
        int base = mBase;
        int next = mNext;
        if(base == next)
        {
            return false;
        }
        out = mData[base];
        mBase = wrap(base+1);
        return true;
    }
private:
    static int wrap( int i )
    {
        while(i>=capacity+1)
        {
            i-=(capacity+1);
        }
        while(i<0)
        {
            i+=(capacity+1);
        }
        return i;
    }
};


template<typename T, int Capacity>
class queue3
{
    T mData[Capacity+1];
    alignas(CacheLineSize) std::atomic<int> mBase;
    alignas(CacheLineSize) std::atomic<int> mNext;
public:
    queue3()
    : mBase(0)
    , mNext(0)
    {}
    bool push_t1(const T& value)
    {
        int base = mBase.load(std::memory_order_relaxed);
        int next = mNext.load(std::memory_order_relaxed);
        int beforemBase = wrap(base-1);
        if(next == beforemBase)
            return false;
        mData[next] = value;
        mNext.store(wrap(next+1),std::memory_order_release); //release
        return true;
    }
    bool pop_t2(T& out)
    {
        int base = mBase.load(std::memory_order_relaxed);
        int next = mNext.load(std::memory_order_acquire); //acquire
        if(base == next)
            return false;
        out = mData[base];
        mBase.store(wrap(base+1), std::memory_order_relaxed);
        return true;
    }
private:
    static int wrap( int i )
    {
        while(i>=Capacity+1)
            i-=(Capacity+1);
        while(i<0)
            i+=(Capacity+1);
        return i;
    }
};


template<typename T, int Capacity>
class queue4
{
    T mData[Capacity+1];
    alignas(CacheLineSize) std::atomic<int> mBase;
    alignas(CacheLineSize) std::atomic<int> mNext;
public:
    queue4()
    : mBase(0)
    , mNext(0)
    {}
    bool push_t1(const T& value)
    {
        int base = mBase.load(std::memory_order_relaxed);
        int next = mNext.load(std::memory_order_relaxed);
        int beforemBase = wrap(base-1);
        if(next == beforemBase)
            return false;
        mData[next] = value;
        mNext.store(wrap(next+1),std::memory_order_release); //release
        return true;
    }
    bool pop_t2(T& out)
    {
        int base = mBase.load(std::memory_order_relaxed);
        int next = mNext.load(std::memory_order_relaxed); //acquire
        if(base == next)
            return false;
        std::atomic_thread_fence(std::memory_order_acquire);
        out = mData[base];
        mBase.store(wrap(base+1), std::memory_order_relaxed);
        return true;
    }
private:
    static int wrap( int i )
    {
        while(i>=Capacity+1)
            i-=(Capacity+1);
        while(i<0)
            i+=(Capacity+1);
        return i;
    }
};


template<typename T, int C>
class queuesingle
{
public:
    typedef T value_type;
    static constexpr int capacity = C;
private:
    value_type mData[capacity+1];
    int mBase;
    int mNext;
public:
    queuesingle()
    : mBase(0)
    , mNext(0)
    {}
    bool push_t1(const value_type& value)
    {
        int beforemBase = wrap(mBase-1);
        if(mNext == beforemBase)
        {
            return false;
        }
        mData[mNext] = value;
        mNext = wrap(mNext+1);
        return true;
    }
    bool pop_t2(value_type& out)
    {
        if(mBase == mNext)
        {
            return false;
        }
        out = mData[mBase];
        mBase = wrap(mBase+1);
        return true;
    }
private:
    static int wrap( int i )
    {
        while(i>=capacity+1)
        {
            i-=(capacity+1);
        }
        while(i<0)
        {
            i+=(capacity+1);
        }
        return i;
    }
};


template<typename T, int Capacity>
class queue_with_mutex
{
    T mData[Capacity+1];
    std::mutex mutex;
    int mBase;
    int mNext;
public:
    queue_with_mutex()
    : mBase(0)
    , mNext(0)
    {}
    bool push_t1(const T& value)
    {
        std::lock_guard<std::mutex> lock(mutex);
        int beforemBase = wrap(mBase-1);
        if(mNext == beforemBase)
        {
            return false;
        }
        mData[mNext] = value;
        mNext = wrap(mNext+1);
        return true;
    }
    bool pop_t2(T& out)
    {
        std::lock_guard<std::mutex> lock(mutex);
        if(mBase == mNext)
        {
            return false;
        }
        out = mData[mBase];
        mBase = wrap(mBase+1);
        return true;
    }
private:
    static int wrap( int i )
    {
        while(i>=Capacity+1)
        {
            i-=(Capacity+1);
        }
        while(i<0)
        {
            i+=(Capacity+1);
        }
        return i;
    }
};



template<typename T, int C>
class queueplay
{
public:
    typedef T value_type;
    static constexpr int capacity = C;
private:
    value_type mData[capacity+1];
    alignas(CacheLineSize) std::atomic<int> mBase;
    alignas(CacheLineSize) std::atomic<int> mNext;
public:
    queueplay()
    : mBase(0)
    , mNext(0)
    {}
    bool push_t1(const value_type& value)
    {
        int base = mBase.load(std::memory_order_relaxed);
        int next = mNext.load(std::memory_order_relaxed);
        int beforemBase = wrap(base-1);
        if(next == beforemBase)
        {
            return false;
        }
        mData[next] = value;
        mNext.store(wrap(next+1),std::memory_order_release); //release
        return true;
    }
    bool pop_t2(value_type& out)
    {
        int base = mBase.load(std::memory_order_relaxed);
        int next = mNext.load(std::memory_order_acquire); //acquire
        if(base == next)
        {
            return false;
        }
        out = mData[base];
        mBase.store(wrap(base+1), std::memory_order_relaxed);
        return true;
    }
private:
    static int wrap( int i )
    {
        while(i>=capacity+1)
        {
            i-=(capacity+1);
        }
        while(i<0)
        {
            i+=(capacity+1);
        }
        return i;
    }
};


#endif //LOCK_FREE_QUEUE_H
