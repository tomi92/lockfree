// main.cpp
#include <iostream>
#include <relacy/relacy_std.hpp>
#include "qsbr.h"

constexpr int TestLength = 10;

struct Pos
{
    rl::var<int> x;
    rl::var<int> y;

    ~Pos()
    {
        assert(x($)==y($));
        x($) = -12;
    }
};

struct race_test : rl::test_suite<race_test, ThreadCount>
{
    rcu::ptr<Pos> rcuPos;
    static thread_local std::forward_list<Pos*> olds;

    void before()
    {
        rcu::createGlobals();
        rcuPos.exchange(new Pos{0,0});
    }

    void thread(unsigned thread_index)
    {
        if (thread_index % 2 == 0)
            myReaderThread();
        else
            myWriterThread();
    }

    void after()
    {
        delete rcuPos.exchange(nullptr);
        rcu::deleteGlobals();
    }

    void invariant(){}

    void myReaderThread()
    {
        rcu::ThreadRegistrator reg;

        for(auto i = 0; i<TestLength; i++)
        {
            rcu::quiescentState();
            if(i%2 == 0)
            {
                rcu::threadOffline();
                //rcu::spin(100);
                rcu::threadOnline();
            }
            rcu::consumer<Pos> pos(rcuPos);
            assert(pos->x($) == pos->y($));
        }
    }

    void myWriterThread()
    {
        rcu::ThreadRegistrator reg;
        for(int i = 1;i<=TestLength; ++i)
        {
            auto oldPtr = rcuPos.exchange(new Pos{i,i});
            rcu::synchronize();
            delete oldPtr;
        }
    }

};

int main()
{
    rl::test_params p;
    p.iteration_count = 100000;
    rl::simulate<race_test>(p);
}
