// qsbr.h
#ifndef QSBR_H
#define QSBR_H

#include <forward_list>
#include <relacy/relacy_std.hpp>

constexpr int ThreadCount = 6;

namespace rcu {

struct noncopyable
{
    noncopyable(){}
private:
    noncopyable( const noncopyable& );
    noncopyable& operator=( const noncopyable& );
};

template<typename T>
void spin(T n)
{
    for(volatile T i=n; i != 0; --i);
}

constexpr uint64_t GpcOffline = 0;
constexpr uint64_t GpcOnline = 1;
constexpr uint64_t GpcIncrement = 2;

using Counter = uint64_t;
using AtomicCounter = rl::atomic<Counter>;

extern AtomicCounter* globalCounter;
extern AtomicCounter* threadLocalCounters;
extern rl::mutex* writeMutex;

void createGlobals();
void deleteGlobals();

inline AtomicCounter& myCounter()
{
    return threadLocalCounters[rl::thread_index()];
}

inline void threadOffline()
{
    myCounter().store(GpcOffline, rl::memory_order_release);
}

inline void threadOnline()
{
    auto tmpGlobalCounter = globalCounter->load(rl::memory_order_acquire);
    myCounter().store(tmpGlobalCounter, rl::memory_order_seq_cst); //cst
}


struct ThreadRegistrator : noncopyable
{
    ThreadRegistrator()
    {
        threadOnline();
    }

    ~ThreadRegistrator()
    {
        threadOffline();
    }
};

inline void readLock()
{}

inline void readUnlock()
{}

inline void quiescentState()
{
    auto tmpGlobalCounter = globalCounter->load(rl::memory_order_acquire);
    myCounter().store(tmpGlobalCounter, rl::memory_order_release);
}

void synchronize();

template<typename T>
class ptr : noncopyable
{
    rl::atomic<T*> ptr_;

    T* consume() const
    {
        auto p =  ptr_.load(rl::memory_order_seq_cst);
        //rl::atomic_thread_fence(rl::memory_order_seq_cst);
        return p;
    }

    template<typename U>
    friend class consumer;

public:

    ptr(T* _ptr = nullptr)
    : ptr_(_ptr)
    {}

    T* exchange(T* _ptr)
    {
        return ptr_.exchange(_ptr, rl::memory_order_seq_cst);
    }
};

template<typename T>
class consumer : noncopyable
{
    T* ptr_;

public:

    consumer(const ptr<T>& _rcu_ptr)
    : ptr_(_rcu_ptr.consume())
    {
        readLock();
    }

    ~consumer()
    {
        readUnlock();
    }

    T* operator->()
    {
        return ptr_;
    }

    T* get() const
    {
        return ptr_;
    }
};

} // namespace rcu

#endif // QSBR_H
