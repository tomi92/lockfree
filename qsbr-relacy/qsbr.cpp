// qsbr.cpp
#include "qsbr.h"

namespace rcu {

AtomicCounter* globalCounter;
AtomicCounter* threadLocalCounters;
rl::mutex* writeMutex;

void createGlobals()
{
    globalCounter = new AtomicCounter(GpcOnline);
    threadLocalCounters = new AtomicCounter[ThreadCount];
    for(int i = 0; i<ThreadCount; ++i)
    {
        threadLocalCounters[i].store(GpcOffline, rl::memory_order_relaxed);
    }
    writeMutex = new rl::mutex();
}

void deleteGlobals()
{
    delete writeMutex;
    delete [] threadLocalCounters;
    delete globalCounter;
}


namespace impl
{
    bool ongoing(AtomicCounter& itsCounter, Counter gpc)
    {
        auto its = itsCounter.load(rl::memory_order_seq_cst);
        return its != GpcOffline && its != gpc;
    }

    void updateCounterAndWait()
    {
        writeMutex->lock($);

        globalCounter->fetch_add(GpcIncrement, rl::memory_order_acq_rel);
        auto gpc = globalCounter->load(rl::memory_order_relaxed);

        for(int i = 0; i<ThreadCount; ++i)
        {
            auto& itsCounter = threadLocalCounters[i];
            while(ongoing(itsCounter, gpc))
                spin(100);
        }

        writeMutex->unlock($);
    }
} //namespace impl

void synchronize()
{
    auto oldCounter = myCounter().load(rl::memory_order_acquire);
    auto wasOnline = oldCounter != GpcOffline;

    if(wasOnline)
    {
        myCounter().store(GpcOffline, rl::memory_order_release);
    }

    impl::updateCounterAndWait();

    if(wasOnline)
    {
        auto gpc = globalCounter->load(rl::memory_order_acquire);
        myCounter().store(gpc, rl::memory_order_release);
    }
}

} //namespace rcu
