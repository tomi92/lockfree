#include <iostream>
#include <forward_list>
#include <algorithm>
#include <chrono>
#include <thread>

std::forward_list<int> list, list1;

template<typename ForwardList, typename Pred>
bool forward_list_remove_if(ForwardList& list, Pred pred)
{
    auto prevIt = std::adjacent_find(list.before_begin(), list.end(),
        [pred](const auto&, const auto& curr){ return pred(curr); });

    bool found = prevIt != list.end();

    if(found)
        list.erase_after(prevIt);

    return found;
}

template<typename ForwardList>
auto forward_list_last(ForwardList& list)
{
    auto it = list.before_begin();
    while(std::next(it)!=list.end())
        ++it;
    return it;
}

template<typename ForwardList>
void forward_list_move_first(ForwardList& from, ForwardList& to)
{
    to.splice_after(to.before_begin(), from, from.before_begin());
}

class nano_timer
{
    using Clock = std::chrono::high_resolution_clock;
    using TimePoint = std::chrono::time_point<Clock>;

    TimePoint t0 = Clock::now();
public:
    uint64_t read()
    {
        using std::chrono::nanoseconds;
        using std::chrono::duration_cast;

        TimePoint t1 = Clock::now();
        return static_cast<uint64_t>(
            duration_cast<nanoseconds>(t1-t0).count());
    }

    void reset()
    {
        t0 = Clock::now();
    }
};

int main()
{
std::cout << sizeof(long) << std::endl;
    list = {4,5,6};
    list1 = {1,2,3};

    nano_timer nt;
    using namespace std::chrono_literals;
    std::this_thread::sleep_for(5s);
    std::cout << nt.read() << std::endl;

    while(!list.empty())
    {
        forward_list_move_first(list, list1);
    }

    for(auto& i: list)
        std::cout << i << std::endl;
    std::cout << std::endl;
    for(auto& i: list1)
        std::cout << i << std::endl;


    return 0;
}
