#!/bin/sh
N=$1
shift
i=0
while [ $i -lt $N ]
do
    $@
    i=$(($i + 1))
done
