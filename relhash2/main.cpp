#include <iostream>
#include <thread>
#include <vector>
#include <cassert>
#include "hashtable.h"

#ifndef READER_THREADS
#define READER_THREADS 3
#endif

class nano_timer
{
    using Clock = std::chrono::high_resolution_clock;
    using TimePoint = std::chrono::time_point<Clock>;

    TimePoint t0 = Clock::now();
public:
    int64_t read()
    {
        using std::chrono::nanoseconds;
        using std::chrono::duration_cast;

        TimePoint t1 = Clock::now();
        return static_cast<int64_t>(
            duration_cast<nanoseconds>(t1-t0).count());
    }

    void reset()
    {
        t0 = Clock::now();
    }
};

rcu::hash_table<int,int> table;
std::atomic<bool> done(false);
int64_t nRead[READER_THREADS];
int64_t maxWait[READER_THREADS];

void reader(int threadNum)
{
    rcu::thread_guard thread_guard;
    int64_t i;
    int64_t myMaxWait = 0;
    for(i = 0; !done; ++i)
    {
        nano_timer nt;
        rcu::readLock();
        const int* p = table.get(i%100);
        if(p != nullptr)
            assert(*p == -(i%100));
        rcu::readUnlock();
        rcu::quiescentState();
        myMaxWait = std::max(myMaxWait, nt.read());
    }
    nRead[threadNum] = i;
    maxWait[threadNum] = myMaxWait;
}

void writer()
{
    rcu::thread_guard thread_guard;
    for(int i = 0; i<1000000; ++i)
    {
        rcu::readLock();
        table.set(i, -i);
        rcu::readUnlock();
        rcu::quiescentState();
    }
    for(int i = 1000000; i>=0; --i)
    {
        rcu::readLock();
        table.remove(i);
        rcu::readUnlock();
        rcu::quiescentState();
    }
    done = true;
}

int main()
{
    rcu::thread_guard thread_guard;
    rcu::readLock();
    rcu::readUnlock();
    std::vector<std::thread> threads;

    for(int i = 0; i<READER_THREADS; ++i)
        threads.emplace_back(reader, i);

    threads.emplace_back(writer);

    rcu::threadOffline();
    for(auto& thread: threads)
        thread.join();
    for(int i = 0;i<READER_THREADS; ++i)
        std::cout << nRead[i] << '\t' << maxWait[i] << std::endl;
}
