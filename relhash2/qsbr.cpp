// qsbr.cpp
#include "qsbr.h"

namespace rcu {
namespace impl {

AtomicCounter globalCounter(GpcOnline);
thread_local AtomicCounter myCounter(GpcOffline);
std::mutex writeMutex;
std::forward_list<AtomicCounter*> threadLocalCounters;

namespace {

bool ongoing(AtomicCounter& itsCounter, Counter gpc)
{
    auto its = itsCounter.load(std::memory_order_seq_cst);
    return its != GpcOffline && its != gpc;
}

void updateCounterAndWait()
{
    std::lock_guard<std::mutex> guard(writeMutex);

    globalCounter.fetch_add(GpcIncrement, std::memory_order_acq_rel);
    auto gpc = globalCounter.load(std::memory_order_relaxed);

    for(auto& itsCounterPtr: threadLocalCounters)
        while(ongoing(*itsCounterPtr, gpc))
            spin(100);
}

} //namespace
} // namespace impl

#ifndef NDEBUG
namespace debug {
namespace impl {
    thread_local bool myIsReadLocked;
}//namespace impl
} //namespace debug
#endif // NDEBUG


void synchronize()
{
    auto oldCounter = impl::myCounter.load(std::memory_order_acquire);
    auto wasOnline = oldCounter != GpcOffline;

    if(wasOnline)
    {
        impl::myCounter.store(GpcOffline, std::memory_order_release);
    }

    impl::updateCounterAndWait();

    if(wasOnline)
    {
        auto gpc = impl::globalCounter.load(std::memory_order_acquire);
        impl::myCounter.store(gpc, std::memory_order_release);
    }
}

} //namespace rcu
