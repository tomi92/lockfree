// qsbr.h
#ifndef QSBR_H
#define QSBR_H

#include <forward_list>
#include <atomic>
#include <mutex>

constexpr int ThreadCount = 6;

namespace rcu {

struct noncopyable
{
    noncopyable(){}
private:
    noncopyable( const noncopyable& );
    noncopyable& operator=( const noncopyable& );
};

template<typename T>
void spin(T n)
{
    for(volatile T i=n; i != 0; --i);
}

//GPC: Grace Period Counter
constexpr uint64_t GpcOffline = 0;
constexpr uint64_t GpcOnline = 1;
constexpr uint64_t GpcIncrement = 2;

using Counter = uint64_t;
using AtomicCounter = std::atomic<Counter>;

namespace impl {
    extern AtomicCounter globalCounter;
    extern thread_local AtomicCounter myCounter;
    extern std::mutex writeMutex;
    extern std::forward_list<AtomicCounter*> threadLocalCounters;
} // namespace impl

#ifndef NDEBUG
namespace debug {

namespace impl {
    extern thread_local bool myIsReadLocked;
}//namespace impl

inline bool isReadLocked()
{
    return impl::myIsReadLocked;
}

} //namespace debug
#endif // NDEBUG

inline void threadOffline()
{
    impl::myCounter.store(GpcOffline, std::memory_order_release);
}

inline void threadOnline()
{
    auto gpc = impl::globalCounter.load(std::memory_order_acquire);
    impl::myCounter.store(gpc, std::memory_order_seq_cst);
}

struct thread_guard : noncopyable
{
    thread_guard()
    {
        std::lock_guard<std::mutex> guard(impl::writeMutex);
        threadOnline();
        impl::threadLocalCounters.push_front(&impl::myCounter);
    }

    ~thread_guard()
    {
        std::lock_guard<std::mutex> guard(impl::writeMutex);
        threadOffline();
        impl::threadLocalCounters.remove(&impl::myCounter);
    }
};

inline void readLock()
{
#ifndef NDEBUG
    debug::impl::myIsReadLocked = true;
#endif // NDEBUG
}

inline void readUnlock()
{
#ifndef NDEBUG
    debug::impl::myIsReadLocked = false;
#endif // NDEBUG
}

inline void quiescentState()
{
    auto gpc = impl::globalCounter.load(std::memory_order_acquire);
    impl::myCounter.store(gpc, std::memory_order_release);
}

void synchronize();

template<typename T>
class ptr : noncopyable
{
    std::atomic<T*> ptr_;

public:

    ptr(T* _ptr = nullptr)
    : ptr_(_ptr)
    {}

    T* exchange(T* _ptr)
    {
        return ptr_.exchange(_ptr, std::memory_order_seq_cst);
    }

    T* consume() const
    {
        return ptr_.load(std::memory_order_seq_cst);
    }

};

template<typename T>
class consumer : noncopyable
{
    T* ptr_;

public:

    consumer(const ptr<T>& _rcu_ptr)
    : ptr_(_rcu_ptr.consume())
    {
        readLock();
    }

    ~consumer()
    {
        readUnlock();
    }

    T* operator->()
    {
        return ptr_;
    }

    T* get() const
    {
        return ptr_;
    }
};

} // namespace rcu

#endif // QSBR_H
