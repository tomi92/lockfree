#ifndef HASHTABLE_H
#define HASHTABLE_H

#include <functional>
#include <memory>
#include <atomic>
#include <list>
#include <vector>
#include <mutex>
#include <shared_mutex>
#include <limits>
#include "qsbr.h"

namespace rcu{

// fejelemes lista létrehozására használható
template<typename T>
struct list_elem
{
    T value;
    rcu::ptr<list_elem> next;

    list_elem(T _value = T(), list_elem* _next = nullptr)
    : value(_value)
    , next(_next)
    {}
};

// todo konstosítás

// must hold rcu read lock or list write lock or owning hashtable write lock
template<typename T, typename Pred>
list_elem<T>* find_after(list_elem<T>* head, Pred pred)
{
    for(auto p = head->next.consume(); p!=nullptr; p=p->next.consume())
    {
        if(pred(p->value))
        {
            return p;
        }
    }
    return nullptr;
}

// must hold rcu read lock or list write lock or owning hashtable write lock
// prev == last && curr == nullptr if not found
template<typename T, typename Pred>
bool find_after(list_elem<T>* head, list_elem<T>*& prev, list_elem<T>*& curr, Pred pred)
{
    prev = head;
    for(curr = head->next.consume(); curr!=nullptr; curr=curr->next.consume())
    {
        if(pred(curr->value))
        {
            return true;
        }
        prev = curr;
    }
    return false;
}

// must hold rcu read lock or list write lock or owning hashtable write lock
template<typename T>
list_elem<T>* last_including(list_elem<T>* head)
{
    auto curr = head;
    auto next = curr->next.consume();

    while(next != nullptr)
    {
        curr = next;
        next = curr->next.consume();
    }

    return curr;
}

template<typename T>
struct list
{
    list_elem<T> head;

    std::recursive_mutex writeMutex;
    using WriteLock = std::lock_guard<std::recursive_mutex>;

    void push_front(const T& value)
    {
        WriteLock lock(writeMutex); // ez kicsit túllő a célon de kit izgat

        auto newElem = new list_elem<T>(value, head.next.consume());
        head.next.exchange(newElem);
    }

    template<typename Pred>
    bool remove(Pred pred)
    {
        WriteLock lock(writeMutex); // ez kicsit túllő a célon de kit izgat

        list_elem<T> *prev, *curr;
        bool found = find(prev, curr, pred);
        if(found)
        {
            prev->next.exchange(curr->next.consume());
            rcu::synchronize();
            delete curr;
        }
        return found;
    }

    template<typename Pred>
    bool try_exchange(Pred pred, const T& value)
    {
        WriteLock lock(writeMutex); // ez kicsit túllő a célon de kit izgat

        list_elem<T> *prev, *curr;
        bool found = find(prev, curr, pred);
        if(found)
        {
            auto newElem = new list_elem<T>(value, curr->next.consume());
            prev->next.exchange(newElem);
            rcu::synchronize();
            delete curr;
        }
        return found;
    }


    template<typename Pred>
    bool exchange_or_push_front(Pred pred, const T& value)
    {
        WriteLock lock(writeMutex);

        bool found = try_exchange(pred, value);
        if(!found)
            push_front(value);
        return found;
    }

    template<typename Pred>
    list_elem<T>* find(Pred pred)
    {
        return find_after(&head, pred);
    }

    template<typename Pred>
    bool find(list_elem<T>*& prev, list_elem<T>*& curr, Pred pred)
    {
        return find_after(&head, prev, curr, pred);
    }

    list_elem<T>* last()
    {
        return last_including(&head);
    }
};

template<typename LockType, typename Mutex, typename Pred, typename Action>
void do_with_double_checked_locking(Mutex& mutex, Pred pred, Action action)
{
    if(pred())
    {
        LockType lock(mutex);
        if(pred())
            action();
    }
}

template<typename K, typename V>
class hash_table
{
    using KV = std::pair<K,V>;
    using Bucket = list<KV>;
    using Table = std::vector<Bucket>;
    using WriteMutex = std::shared_timed_mutex; // todo change to shared_mutex when c++17 support is around
    using SharedWriteLock = std::shared_lock<std::shared_timed_mutex>;
    using UniqueWriteLock = std::unique_lock<std::shared_timed_mutex>;
    static constexpr size_t MinTableSize = 16;

    struct KeyEquals
    {
        K key;

        KeyEquals(K _key)
        : key(_key)
        {}

        bool operator()(const KV& kv) const
        {
            return kv.first == key;
        }
    };

    std::hash<K> hash_;
    rcu::ptr<Table> table_;
    WriteMutex writeMutex_;
    std::atomic<size_t> size_;

    Bucket& getBucketOf(const K& key) const
    {
        auto& table = *table_.consume();
        return table[hash_(key) % table.size()];
    }

    bool overPopulated()
    {
        auto table = table_.consume();
        auto size = size_.load();
        return size >= table->size() * 0.7;
    }

    bool underPopulated()
    {
        auto table = table_.consume();
        auto size = size_.load();
        return size <= table->size() * 0.2 && size > MinTableSize;
    }

    // must hold UniqueWriteLock
    void shrink_to_half_impl()
    {
        Table* oldTable = table_.consume();
        assert(oldTable->size()/2 >= 1);
        Table* newTable = new Table(oldTable->size()/2);
        for(size_t i = 0; i<newTable->size(); ++i)
        {
            (*newTable)[i].head.next.exchange((*oldTable)[i].head.next.consume());
            (*newTable)[i].last()->next.exchange(
                (*oldTable)[i+newTable->size()].head.next.consume());
        }
        table_.exchange(newTable);
        rcu::synchronize();
        delete oldTable;
    }

    void link_new_bucket_to_first_fitting(Table& oldTable, size_t oldBucketId, Table& newTable, size_t newBucketId)
    {
        auto firstGood = oldTable[oldBucketId].find([&](const KV& kv){
            return hash_(kv.first) % newTable.size() == newBucketId;
        });

        if(firstGood != nullptr)
            newTable[newBucketId].head.next.exchange(firstGood);
    }

    // returns false if no more unzip is needed
    bool unzip_step(Table& oldTable, size_t newTableSize)
    {
        bool hadToUnzip = false;
        for(size_t i = 0; i<oldTable.size(); ++i)
        {
            auto& bucket = oldTable[i];
            auto* first = bucket.head.next.consume();

            if(first  == nullptr)
                continue;

            auto newBucketOfFirst = hash_(first->value.first) % newTableSize;

            list_elem<KV> *prev, *firstBad; //todo element type def

            find_after(first, prev, firstBad, [&](const KV& kv){
                return hash_(kv.first) % newTableSize != newBucketOfFirst;
            });

            bucket.head.next.exchange(firstBad);

            if(firstBad == nullptr)
                continue;

            hadToUnzip = true;

            auto* firstGoodAfterBad = find_after(firstBad, [&](const KV& kv){
                return hash_(kv.first) % newTableSize == newBucketOfFirst;
            });

            prev->next.exchange(firstGoodAfterBad);
        }
        return hadToUnzip;
    }

    // must hold uwl
    void expand_to_double_impl()
    {
        Table* oldTable = table_.consume();
        assert(oldTable->size() <= std::numeric_limits<size_t>::max() / 2);

        Table* newTable = new Table(2*oldTable->size());

        for(size_t i = 0; i<oldTable->size(); ++i)
        {
            link_new_bucket_to_first_fitting(*oldTable, i, *newTable, i);
            link_new_bucket_to_first_fitting(*oldTable, i, *newTable, oldTable->size() + i);
        }

        table_.exchange(newTable);
        rcu::synchronize();

        while(unzip_step(*oldTable, newTable->size()))
            rcu::synchronize();

        delete oldTable;
    }

public:

    hash_table(int initialBuckets = MinTableSize) // todo feltétel a méretre
    : table_(new Table(initialBuckets))
    {}

    ~hash_table()
    {
        delete table_.consume();
        // todo delete list contents
    }

    // must be in read-side-critical-section
    const V* get(const K& key) const
    {
        assert(debug::isReadLocked());
        auto& bucket = getBucketOf(key);
        auto* elem = bucket.find(KeyEquals(key));
        return elem == nullptr ? nullptr : &elem->value.second;
    }

    void set(const K& key, const V& value)
    {
        bool wasPresent;

        {
            SharedWriteLock swl(writeMutex_);
            assert(debug::isReadLocked());
            auto& bucket = getBucketOf(key);
            wasPresent = bucket.exchange_or_push_front(KeyEquals(key), KV(key, value));
        }

        if(!wasPresent)
        {
            size_.fetch_add(1);
            do_with_double_checked_locking<UniqueWriteLock>(
                writeMutex_,
                [this]{return overPopulated();},
                [this]{expand_to_double_impl();}
            );
        }
    }

    void remove(const K& key)
    {
        bool wasPresent;

        {
            SharedWriteLock swl(writeMutex_);
            assert(debug::isReadLocked());
            auto& bucket = getBucketOf(key);
            wasPresent = bucket.remove(KeyEquals(key));
        }

        if(wasPresent)
        {
            size_.fetch_sub(1);
            do_with_double_checked_locking<UniqueWriteLock>(
                writeMutex_,
                [this]{return underPopulated();},
                [this]{shrink_to_half_impl();}
            );
        }
    }
};

} //namespace rcu

#endif //HASHTABLE_H
