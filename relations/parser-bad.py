from pyparsing import *

id = Word('ABCDEFGHIJKLMNOPQRSTUVWXYZ', '0123456789')

cdop = Literal(';d') | Literal('cd')
cdex = Forward()
cdex << (id + cdop + (cdex | id))

sbop = cdop | Literal(';') | Literal('sb')
sbex = Forward()
sbex << (id + sbop + (sbex | id))

swop = Literal('<s') | Literal('sw')
swex = id + swop + id

doop = Literal('<d') | Literal('do')
doex = Forward()
doex << (id + doop + (cdex | id))
doex = doex + cdop + id

itop = Literal('<i') | Literal('it')
itex = Forward()
itex << ((id + swop + (itex | sbex | id)) | (id + doop + (itex | doex | id)) | (id + sbop + itex))

"""doop = Literal('<d') | Literal('do')
doex = id + doop + id
doex = doex + cdop + id
itop = Literal('<i') | Literal('it')
itex = id + itop + id
itex = swex
itex = doex
itex = swex + sbop + (id | sbex)
itex = itex + itop + id
itex = (id | sbex) + sbop + itex 
hbop = Literal('<h') | Literal('hb')
hbex = id + hbop + id
hbex = (sbex | itex)
"""
tokens = itex.parseString("A ; C <s D <s ")
print tokens
