/* GNU Prolog */

:-dynamic(sbf/2).
:-dynamic(cdtf/2).
:-dynamic(swf/2).
:-dynamic(dobf/2).
:-dynamic(ithbf/2).
:-dynamic(hbf/2).

sb(A, B) :- sbf(A, B).
sb(A, B) :- cdt(A, B).

cdt(A, B) :- cdtf(A, B).
cdt(A, B) :- cdtf(A, X), cdt(X, B).

sw(A, B) :- swf(A, B).

dob(A, B) :- dobf(A, B).
dob(A, B) :- dobf(A, X), cdt(X, B).

ithb1(A, B) :- ithbf(A, B).
ithb1(A, B) :- sw(A, B).
ithb1(A, B) :- dob(A, B).
ithb1(A, B) :- sw(A, X), sb(X, B).
ithb1(A, B) :- sb(A, X), ithb1(X, B).
ithb(A, B) :- ithb1(A, B).
ithb(A, B) :- ithb1(A, X), ithb(X, B).

hb(A, B) :- hbf(A, B).
hb(A, B) :- sb(A, B).
hb(A, B) :- ithb(A, B).
