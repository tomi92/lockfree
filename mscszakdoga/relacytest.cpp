#include <relacy/relacy_std.hpp>
#include <memory>
#include "queue.h"

using TestType = rl::var<int64_t>;
constexpr int TestSize = 10;
using QueueType = queue<TestType,TestSize>();

struct race_test : rl::test_suite<race_test, 2>
{
    std::unique_ptr<QueueType> q;
    
    // egy szálban hajtódik végre a thread függvény előtt
    void before()
    {
        q = std::make_unique<QueueType>(); // ehhez c++14 kell
    }

    void thread(unsigned thread_index)
    {
        if (thread_index == 0)
            t1();
        else
            t2();
    }
        
    void t1()
    {
        for(int i = 0; i<5*TestSize; i++)
            while(!q->push_t1(i));
    }

    void t2()
    {
        for(int i = 0; i<5*TestSize; i++)
            TestType j=-1;
            while(!q->pop_t2(j));
            assert(i == j($));
    }    

    // egy szálban hajtódik végre a thread függvény után
    void after()
    { }

    // egy szálban hajtódik végre minden vizsgált művelet után
    void invariant()
    { }
};

int main()
{
    rl::simulate<race_test>();
}
